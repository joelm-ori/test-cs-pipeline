FROM alpine:latest

RUN addgroup -S cs && adduser -S -G cs cs
RUN apk add --no-cache bash

WORKDIR /app
COPY scripts/ /app

USER cs

CMD ["sh", "-c", "tail -f /dev/null"]
