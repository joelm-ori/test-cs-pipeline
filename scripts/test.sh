#!/bin/bash

# Function to clear the screen
clear_screen() {
    clear
}

# Function to generate a random pattern of characters
generate_random_pattern() {
    chars=("!" "@" "#" "$" "%" "&" "*" "+" "-" "=")
    for i in {1..30}; do
        printf "%s" "${chars[RANDOM % ${#chars[@]}]}"
    done
    echo
}

# Function to display the "||Customer Success||" animation
cs_animation() {
    for i in {1..5}; do
        generate_random_pattern
        echo "              ||Customer Success||              "
        generate_random_pattern
        sleep 0.5
        clear_screen
        sleep 0.5
    done
}

# Run the animation
cs_animation

